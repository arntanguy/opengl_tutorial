#include <GL/glew.h>
#include <SFML/Window.hpp>

#include <iostream>

int main(int argc, char *argv[])
{
  // Settings for OpenGL context
  sf::ContextSettings settings;
  settings.depthBits = 24;
  settings.stencilBits = 8;
  settings.antialiasingLevel = 2;  // Optional
  settings.majorVersion = 4;
  settings.minorVersion = 5;

  // Create Window and context
  sf::Window window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Close,
                    settings);

  /**
   * The graphics card vendor is responsible to implement OpenGL
   * functionnalities in their drivers based on what the graphics card supports.
   * GLEW takes care of initializing all the required function pointers to
   * the actual driver implementation. This is required to use any
   * modern OpenGL features
   */
  glewExperimental = GL_TRUE;
  glewInit();

  /**
   * Creates a simple empty vertex buffer, to text the context creation
   **/
  GLuint vertexBuffer;
  glGenBuffers(1, &vertexBuffer);
  std::cout << "Vertex buffer created with id: " << vertexBuffer << std::endl;


  /**
   * Event loop for SFML window
   **/
  bool running = true;
  while (running)
  {
    glClear(GL_COLOR_BUFFER_BIT);

    sf::Event windowEvent;
    while (window.pollEvent(windowEvent))
    {
      switch (windowEvent.type)
      {
        case sf::Event::Closed:
          running = false;
          break;
        case sf::Event::KeyPressed:
          if (windowEvent.key.code == sf::Keyboard::Escape) running = false;
          break;
        default:
          break;
      }
    }
    window.display();
  }

  return 0;
}
