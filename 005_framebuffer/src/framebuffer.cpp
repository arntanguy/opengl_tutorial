#include <GL/glew.h>
#include <SFML/Window.hpp>

#include <iostream>
#include <chrono>

#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>

int main(int argc, char *argv[])
{
  // Settings for OpenGL context
  sf::ContextSettings settings;
  settings.depthBits = 24;
  settings.stencilBits = 8;
  settings.antialiasingLevel = 2;  // Optional
  settings.majorVersion = 4;
  settings.minorVersion = 5;

  // Create Window and context
  sf::Window window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Close,
                    settings);

  // Initialize OpenGL functionalities
  glewExperimental = GL_TRUE;
  glewInit();

  /**
   * Declare the data to be drawn on the CPU
   * Here, we declare a triangle defined by 3 2D vertices.
   **/
  std::array<float, 4 * 8> vertices{
      {// X      Y     Z     R     G     B     U     V
       -0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.5f, 0.0f, 0.0f,
       1.0f, 0.0f, 1.0f, 0.0f, 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
       -0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f}};

  // Element array references the vertices by index
  // 0 is the top-left vertex, 1 the top-right...
  std::array<int, 2 * 3> elements{{0, 1, 2, 2, 3, 0}};

  // Create VAO to store how the data is laid out
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Create EBO (element array buffer)
  GLuint ebo;
  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements.data(),
               GL_STATIC_DRAW);

  // Create VBO for framebuffer rendering
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(),
               GL_STATIC_DRAW);

  /**
   * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   * Create a FrameBuffer
   * Framebuffers objects allow you to render a scene directly to a texture.
   * They are incredibly useful for post-processing, and retrieving information
   * about the rendered scene (color, depthmap, stencil buffer...)
   * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   **/
  GLuint frameBuffer;
  glGenFramebuffers(1, &frameBuffer);
  // At that point the framebuffer object is created, but is not complete yet
  // and can't be used.
  // For a framebuffer to be complete, there needs to be at least one buffer
  // attached (color, depth, stencil...), and at least one color attachement.
  // Besides, all attachement need to be complete (a texture attachement needs
  // to have its memory reserved...), and all attachements must have the same
  // number of multisamples.

  // Bind the framebuffer to work with it.
  glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

  {  // You can test whether a framebuffer is complete as follows
    GLuint fb_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (fb_status != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cout << "Framebuffer " << frameBuffer
                << " is still incomplete and cannot yet be used." << std::endl;
    }
  }

  // =========================
  // Framebuffer attachements
  // =========================
  // The framebuffer can only be used as a render target if memory has been
  // allocated to store the results. This is done by attaching images for each
  // buffer (color, depth, stencil...).
  // For that purpose, two kind of objects can be used:
  // - Texture Objects: may be used directly in subsequent shaders
  // - Render Buffers: might be more optimized for rendering, but cannot be used
  // directly in shaders
  GLuint texColorBuffer;
  glGenTextures(1, &texColorBuffer);
  glBindTexture(GL_TEXTURE_2D, texColorBuffer);

  // Note the nullptr for the data parameter. The data will be directly rendered
  // on the GPU, so no data needs to be uploaded to the texture.
  // Width and height should match the desired rendering viewport
  // (glViewport...)
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 800, 600, 0, GL_RGB, GL_UNSIGNED_BYTE,
               nullptr);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  // Attach the texture to the framebuffer. All color rendered while the
  // framebuffer is active will now be written on texColorBuffer texture
  // instead of on the screen.
  // Note the second parameter implies that you can have multiple color
  // attachements
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         texColorBuffer, 0);

  {  // You can test whether a framebuffer is complete as follows
    GLuint fb_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (fb_status != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cout << "Framebuffer " << frameBuffer
                << " is still incomplete and cannot yet be used." << std::endl;
    }
    else
    {
      std::cout << "Framebuffer is now complete with one color attachement"
                << std::endl;
    }
  }
  // Render depthmap and stencil buffer into a a renderbuffer object.
  // This could have been rendered slightly less efficiently to texture instead
  GLuint rboDepthStencil;
  glGenRenderbuffers(1, &rboDepthStencil);
  glBindRenderbuffer(GL_RENDERBUFFER, rboDepthStencil);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800, 600);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                            GL_RENDERBUFFER, rboDepthStencil);

  {  // You can test whether a framebuffer is complete as follows
    GLuint fb_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (fb_status != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cout << "Framebuffer " << frameBuffer
                << " is still incomplete and cannot yet be used." << std::endl;
    }
    else
    {
      std::cout << "Framebuffer is now complete with one color attachement, "
                   "and one render buffer for depth and stencil" << std::endl;
    }
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  // Setting up framebuffer done

  /**
   * DEFINING SHADERS - Rendering pipeline
   **/

  // 1 - VERTEX SHADER

  std::string vertexSource =
      "#version 440\n"
      "layout(location=0) in vec3 position;"
      "layout(location=1) in vec3 color;"
      "layout(location=2) in vec2 texcoord;"
      "out vec3 Color;"
      "out vec2 Texcoord;"
      "layout(location=1) uniform mat4 trans;"
      "void main()"
      "{"
      "    Texcoord = texcoord;"
      "    Color = color;"
      "    gl_Position = trans * vec4(position, 1.0);"
      "}";
  const char *vertexSource_str = vertexSource.c_str();

  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexSource_str, NULL);
  glCompileShader(vertexShader);

  {  // debug shader compilation
    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE)
    {
      char buffer[512];
      glGetShaderInfoLog(vertexShader, 512, NULL, buffer);
      std::cerr << "Error compiling vertex shader: " << buffer << std::endl;
    }
  }

  // 2 - FRAGMENT SHADER
  std::string fragmentSource =
      "#version 440\n"
      "in vec3 Color;"
      "in vec2 Texcoord;"
      "out vec4 outColor;"
      "layout(location=0) uniform float intensity;"
      "layout(binding=0) uniform sampler2D tex1;"
      "layout(binding=1) uniform sampler2D tex2;"
      "void main()"
      "{"
      "		outColor = mix(textureLod(tex1, Texcoord, 0), "
      "textureLod(tex2, Texcoord, 0), intensity);"
      "}";
  const char *fragmentSource_str = fragmentSource.c_str();

  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentSource_str, NULL);
  glCompileShader(fragmentShader);

  {  // debug shader compilation
    GLint status;
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE)
    {
      char buffer[512];
      glGetShaderInfoLog(fragmentShader, 512, NULL, buffer);
      std::cerr << "Error compiling fragment shader: " << buffer << std::endl;
    }
  }

  // 3 - PROGRAM
  GLuint shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glBindFragDataLocation(shaderProgram, 0, "outColor");
  glLinkProgram(shaderProgram);
  glUseProgram(shaderProgram);

  // layout id of the position attribute in the vertex shader.
  const GLint posAttrib = 0;
  const GLint colorAttrib = 1;
  const GLint texAttrib = 2;

  /**
   * VERTEX ATTRIBUTE POINTERS
   **/
  // Enable the vertex attribute
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);
  // Enable the color attribute
  glEnableVertexAttribArray(colorAttrib);
  glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                        (void *)(3 * sizeof(float)));

  glEnableVertexAttribArray(texAttrib);
  glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                        (void *)(6 * sizeof(float)));

  /**
   * TEXTURE
   * Just like VBOs, textures are special buffers on the GPU
   **/
  GLuint tex[2];
  glGenTextures(2, tex);

  // Textures can be 1D, 2D, or 3D
  // Activate texture unit 0
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex[0]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);

  cv::Mat image = cv::imread("../resources/color.png");
  if (image.empty())
  {
    std::cout << "image empty" << std::endl;
  }
  else
  {
    glTexImage2D(
        GL_TEXTURE_2D,  // Type of texture
        0,  // Pyramid level (for mip-mapping) - 0 is the top level (highest
            // resolution)
        GL_RGB,      // Internal colour format to convert to
        image.cols,  // Image width  i.e. 640 for Kinect in standard mode
        image.rows,  // Image height i.e. 480 for Kinect in standard mode
        0,           // Border width in pixels (can either be 1 or 0)
        GL_BGR,      // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
        GL_UNSIGNED_BYTE,  // Image data type
        image.ptr());      // The actual image data itself

    glGenerateMipmap(GL_TEXTURE_2D);
  }

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex[1]);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);

  cv::Mat image2 = cv::imread("../resources/color1.png");
  if (image2.empty())
  {
    std::cout << "image empty" << std::endl;
  }
  else
  {
    glTexImage2D(
        GL_TEXTURE_2D,  // Type of texture
        0,  // Pyramid level (for mip-mapping) - 0 is the top level (highest
            // resolution)
        GL_RGB,       // Internal colour format to convert to
        image2.cols,  // Image width  i.e. 640 for Kinect in standard mode
        image2.rows,  // Image height i.e. 480 for Kinect in standard mode
        0,            // Border width in pixels (can either be 1 or 0)
        GL_BGR,       // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
        GL_UNSIGNED_BYTE,  // Image data type
        image2.ptr());     // The actual image data itself

    glGenerateMipmap(GL_TEXTURE_2D);
  }

  const GLchar *screenVertexSource =
      "#version 440\n"
      "void main()"
      "{"
      "}";
  // blur shader
  const GLchar *screenFragmentSource =
      "#version 440\n"
      "in vec2 texcoord;"
      "out vec4 outColor;"
      "uniform sampler2D texFramebuffer;"
      "const float blurSizeH = 1.0 / 800.0;"
      "const float blurSizeV = 1.0 / 600.0;"
      "void main()"
      "{"
      " vec4 sum = vec4(0.0);"
      " for (int x = -4; x <= 4; x++)"
      "     for (int y = -4; y <= 4; y++)"
      "         sum += texture("
      "             texFramebuffer,"
      "             vec2(texcoord.x + x * blurSizeH, texcoord.y + y * "
      "blurSizeV)"
      "         ) / 81.0;"
      " outColor = sum;"
      "}";
  // Geometry shader: emits a fullscreen quad
  const GLchar *screenFullscreenQuadSource =
      "#version 440\n"
      "layout(points) in;"
      "layout(triangle_strip, max_vertices = 4) out;"
      ""
      "out vec2 texcoord;"
      ""
      "void main() "
      "{"
      "    gl_Position = vec4( 1.0, 1.0, 0., 1.0 );"
      "    texcoord = vec2( 1.0, 1.0 );"
      "    EmitVertex();"
      ""
      "    gl_Position = vec4(-1.0, 1.0, 0., 1.0 );"
      "    texcoord = vec2( 0.0, 1.0 ); "
      "    EmitVertex();"
      ""
      "    gl_Position = vec4( 1.0,-1.0, 0., 1.0 );"
      "    texcoord = vec2( 1.0, 0.0 ); "
      "    EmitVertex();"
      ""
      "    gl_Position = vec4(-1.0,-1.0, 0., 1.0 );"
      "    texcoord = vec2( 0.0, 0.0 ); "
      "    EmitVertex();"
      "    EndPrimitive(); "
      "}";

  GLuint vertexShaderScreen = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShaderScreen, 1, &screenVertexSource, NULL);
  glCompileShader(vertexShaderScreen);

  /**
   * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   * Geometry shaders are an extra-step of the rendering pipeline, that
   * can be used for altering the geometry providing from vertex shaders.
   * It may:
   * - Emit geometry : add triangles, points...
   * - Remove geometry : discard vertices, triangles...
   * Geometry shaders have many uses, such as handling level of detail for large
   * terrains and meshes. Here, we use it to render a fullscreen quad, on which
   * the framebuffer texture will be displayed.
   *
   * This could have also been done using a VBO and EBO containing the
   * fullscreen quad vertices, and a vertex shader, as done in previous
   * tutorials. However, using a geometryShader saves a bit of GPU memory,
   * and reduces the amount of work needed by the CPU.
   * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   **/
  GLuint geometryShaderQuad = glCreateShader(GL_GEOMETRY_SHADER);
  glShaderSource(geometryShaderQuad, 1, &screenFullscreenQuadSource, NULL);
  glCompileShader(geometryShaderQuad);

  GLuint fragmentShaderScreen = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShaderScreen, 1, &screenFragmentSource, NULL);
  glCompileShader(fragmentShaderScreen);

  GLuint shaderProgramScreen = glCreateProgram();
  glAttachShader(shaderProgramScreen, vertexShaderScreen);
  glAttachShader(shaderProgramScreen, geometryShaderQuad);
  glAttachShader(shaderProgramScreen, fragmentShaderScreen);
  glBindFragDataLocation(shaderProgramScreen, 0, "outColor");
  glLinkProgram(shaderProgramScreen);

  const GLint uniIntensity = 0;
  glUniform1f(uniIntensity, 1.f);

  const GLint uniTrans = 1;

  // Intrinsic projection for the kinect
  Eigen::Projective3f K_virtual;
  K_virtual.matrix() << 0.8283, 0, 0.4992, 0, 0, 1.0938, 0.4990, 0, 0, 0, 1, 0,
      0, 0, 0, 1;

  // View: world-space pose of the camera
  Eigen::Matrix4f V = Eigen::Matrix4f::Identity();
  // Model: model-space transformation
  Eigen::Matrix4f M = Eigen::Matrix4f::Identity();
  Eigen::Matrix4f P = Eigen::Matrix4f::Identity();

  /**
   * Event loop for SFML window
   **/
  auto t_start = std::chrono::high_resolution_clock::now();
  bool running = true;
  while (running)
  {
    sf::Event windowEvent;
    while (window.pollEvent(windowEvent))
    {
      switch (windowEvent.type)
      {
        case sf::Event::Closed:
          running = false;
          break;
        case sf::Event::KeyPressed:
          if (windowEvent.key.code == sf::Keyboard::Escape)
            running = false;
          else if (windowEvent.key.code == sf::Keyboard::Q)
            V(0, 3) -= .1;
          else if (windowEvent.key.code == sf::Keyboard::D)
            V(0, 3) += .1;
          else if (windowEvent.key.code == sf::Keyboard::Z)
            V(1, 3) += .1;
          else if (windowEvent.key.code == sf::Keyboard::S)
            V(1, 3) -= .1;
          break;
        default:
          break;
      }
    }

    /**
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * This time the rendering is done in two stages:
     * 1 - The Framebuffer is activated and our scene is rendered on it
     * 2 - The default framebuffer (screen) is activated, on the scene rendered
     * on the framebuffer of scene 1 is applied on a fullscreen quad, with
     * additional blurring.
     *
     * Notice that this time, it is needed to activate the correct VAOs and
     * textures so that the shaders have the expected inputs.
     * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     **/

    // Bind the default framebuffer (render on screen)
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    // Clear the screen to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Activate the scene rendering program, with appropiate textures active
    glUseProgram(shaderProgram);
    glBindVertexArray(vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex[0]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex[1]);

    auto t_now = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration_cast<std::chrono::duration<float>>(
                     t_now - t_start).count();
    // Modify the fragement shader uniform for intensity
    // This will cause the triangle to gradually appear and disappear over time
    auto val = (sin(time / 2.f) + 1.f) / 2.f;
    glUniform1f(uniIntensity, val);

    Eigen::Matrix3f m;
    m = Eigen::AngleAxisf(0. * M_PI, Eigen::Vector3f::UnitX()) *
        Eigen::AngleAxisf(val * M_PI, Eigen::Vector3f::UnitY()) *
        Eigen::AngleAxisf(0.33 * M_PI, Eigen::Vector3f::UnitZ());
    M << m, Eigen::Vector3f(0, 0, 0), 0, 0, 0, 1;
    // NOTE: in computer graphics, it is standard to compute the MVP directly in
    // the shader instead of on the GPU.
    P = K_virtual * V * M;
    glUniformMatrix4fv(uniTrans, 1, GL_FALSE, P.data());

    glEnable(GL_DEPTH_TEST);
    // Draws the currently bound VAO using the currently bound shader program.
    // This is drawn on the active framebuffer, which means that here we are
    // doing rendering to texture.
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glDisable(GL_DEPTH_TEST);

    // Bind the default framebuffer for on-screen rendering
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // Clear the screen to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindVertexArray(0);
    glUseProgram(shaderProgramScreen);
    // Bind the color texture of the framebuffer, where we have rendered the
    // scene
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texColorBuffer);
    // Dummy call to activate geometry shader: emit one point, and the geometry
    // shader will take care of emitting the required fullscreen quad.
    glDrawArrays(GL_POINTS, 0, 1);

    window.display();
  }

  glDeleteTextures(2, tex);

  // Clean up
  glDeleteProgram(shaderProgram);
  glDeleteShader(fragmentShader);
  glDeleteShader(vertexShader);
  glDeleteBuffers(1, &vbo);
  glDeleteVertexArrays(1, &vao);
  glDeleteFramebuffers(1, &frameBuffer);

  return 0;
}
