# Framebuffer rendering 

This tutorial is based, on https://open.gl/framebuffers

The main difference here is that a Geometry Shader is used to render the final fullscreen quad instead 
of a VBO. This is both more efficient and flexible, and gently introduces the concept of geometry shader.
