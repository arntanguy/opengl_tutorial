# Direct projection pipeline 

This tutorial is based, on https://open.gl/transformations 
However, it uses Eigen3 instead of GLM, and expresses the projection
pipeline using camera calibration matrices
