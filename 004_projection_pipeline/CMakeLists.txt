cmake_minimum_required(VERSION 2.8)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/../cmake")
project(opengl_tutorial_projection_pipeline)

# For YouCompleteMe and Clang compilation
set( CMAKE_EXPORT_COMPILE_COMMANDS 1 )

add_definitions(-std=c++11 -g -Wall -Wno-comment -Wno-unused-local-typedefs)

find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(SFML 2 REQUIRED system window graphics)
find_package(OpenCV REQUIRED)
find_package(Eigen3 REQUIRED)

add_subdirectory(src)
