#include <GL/glew.h>
#include <SFML/Window.hpp>

#include <iostream>
#include <chrono>

#include <opencv2/opencv.hpp>

int main(int argc, char *argv[])
{
  // Settings for OpenGL context
  sf::ContextSettings settings;
  settings.depthBits = 24;
  settings.stencilBits = 8;
  settings.antialiasingLevel = 2;  // Optional
  settings.majorVersion = 4;
  settings.minorVersion = 5;

  // Create Window and context
  sf::Window window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Close,
                    settings);

  // Initialize OpenGL functionalities
  glewExperimental = GL_TRUE;
  glewInit();

  /**
   * Declare the data to be drawn on the CPU
   * Here, we declare a triangle defined by 3 2D vertices.
   **/
  std::array<float, 4 * 7> vertices{{
      //  Position   Color             Texcoords
      -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  // Top-left
      0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 2.0f, 0.0f,   // Top-right
      0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 2.0f, 2.0f,  // Bottom-right
      -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 2.0f  // Bottom-left
  }};

  // Element array references the vertices by index
  // 0 is the top-left vertex, 1 the top-right...
  std::array<int, 2 * 3> elements{{0, 1, 2, 2, 3, 0}};

  // Create VAO to store how the data is laid out
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Create EBO (element array buffer)
  GLuint ebo;
  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements.data(),
               GL_STATIC_DRAW);

  // Create VBO
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(),
               GL_STATIC_DRAW);

  /**
   * DEFINING SHADERS - Rendering pipeline
   **/

  // 1 - VERTEX SHADER

  std::string vertexSource =
      "#version 440\n"
      "layout(location=0) in vec2 position;"
      "layout(location=1) in vec3 color;"
      "layout(location=2) in vec2 texcoord;"
      "out vec3 Color;"
      "out vec2 Texcoord;"
      "void main()"
      "{"
      "    Texcoord = texcoord;"
      "    Color = color;"
      "    gl_Position = vec4(position, 0.0, 1.0);"
      "}";
  const char *vertexSource_str = vertexSource.c_str();

  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexSource_str, NULL);
  glCompileShader(vertexShader);

  {  // debug shader compilation
    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE)
    {
      char buffer[512];
      glGetShaderInfoLog(vertexShader, 512, NULL, buffer);
      std::cerr << "Error compiling vertex shader: " << buffer << std::endl;
    }
  }

  // 2 - FRAGMENT SHADER
  std::string fragmentSource =
      "#version 440\n"
      "in vec3 Color;"
      "in vec2 Texcoord;"
      "out vec4 outColor;"
      "layout(location=0) uniform float intensity;"
      "layout(binding=0) uniform sampler2D tex1;"
      "layout(binding=1) uniform sampler2D tex2;"
      "void main()"
      "{"
      "		outColor = mix(textureLod(tex1, Texcoord, 0), textureLod(tex2, Texcoord, 0), intensity);"
      "}";
  const char *fragmentSource_str = fragmentSource.c_str();

  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentSource_str, NULL);
  glCompileShader(fragmentShader);

  {  // debug shader compilation
    GLint status;
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE)
    {
      char buffer[512];
      glGetShaderInfoLog(fragmentShader, 512, NULL, buffer);
      std::cerr << "Error compiling fragment shader: " << buffer << std::endl;
    }
  }

  // 3 - PROGRAM
  GLuint shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glBindFragDataLocation(shaderProgram, 0, "outColor");
  glLinkProgram(shaderProgram);
  glUseProgram(shaderProgram);

  // layout id of the position attribute in the vertex shader.
  const GLint posAttrib = 0;
  const GLint colorAttrib = 1;
  const GLint texAttrib = 2;

  /**
   * VERTEX ATTRIBUTE POINTERS
   * Vertex Attribute pointers are at first sight rather confusing.
   * Their role is to specify how the input data is laid out.
   * They specify how to use data in the currently bound VBO
   * (here "vbo" containing our triangle vertices).
   * It takes as parameters:
   *  - The reference to the input attribute
   *  - The number of values for that input, which is the same as the number of
   *    compoments of the vec
   *  - The type of each component (GL_FLOAT)
   *  - Whether each component should be normalized between -1 and 1
   *  - The stride: how many bytes are between each position attribute
   *    in the array
   *  - The offset: how many bytes from the start of the array
   *    the attribute occurs
   *
   * IMPORTANT NOTE: this function will not only store the stride and offset,
   * but also the VBO currently bound to GL_ARRAY_BUFFER.
   * This means the the VBO doesn't have to be explicitely bound when the actual
   * drawing functions are called.
   * This also means that you can have a different VBO for each attribute.
   **/

  // We just need to alter the attribute pointer code a bit to accommodate for
  // the new
  // X, Y, R, G, B attribute order.
  // The fifth parameter is set to 5*sizeof(float) now,
  // because each vertex consists of 5 floating point attribute values.
  // The offset of 2*sizeof(float) for the color attribute is there because each
  // vertex starts
  // with 2 floating point values for the position that it has to skip over.

  // Enable the vertex attribute
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(float), 0);
  // Enable the color attribute
  glEnableVertexAttribArray(colorAttrib);
  glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(float),
                        (void *)(2 * sizeof(float)));

  glEnableVertexAttribArray(texAttrib);
  glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(float),
                        (void *)(5 * sizeof(float)));

  /**
   * TEXTURE
   * Just like VBOs, textures are special buffers on the GPU
   **/
  GLuint tex[2];
  glGenTextures(2, tex);

  // Textures can be 1D, 2D, or 3D
  // Activate texture unit 0
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex[0]);

  // SAMPLING
  // First, we need to define a sampling mode for the images.
  // Sampling modes can be GL_REPEAT, GL_MIRRORED_REPEAT, GL_CLAMP_TO_EDGE,
  // GL_CLAMP_TO_BORDER
  // It controls what value is returned when using a value outside of the
  // normalized image range (ie a value that ought to fall outside of the
  // boundary of the image).
  // This is particularely useful in computer vision where many algorithms, such
  // as convolutions, warping... usually need data outside of the image borders.
  // S, T, R are the normalized UV coordinates of the texture
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  // FILTERING
  // Since texture coordinates are normalized, they won't always match a pixel
  // perfectly. OpenGL has various modes for deciding how to interpolate the
  // color for the desired UV coordinates.
  // GL_NEAREST: closest of the 4 pixel to the coordinate
  // GL_LINEAR: weighted average of the 4 surrounding pixels
  // GL_*MIPMAP* : sample from mipmaps instead
  // You can specify the interpolation for two cases: scaling the image down and
  // scaling it up
  // In practice, the MIMAP version will be most oftenly used, as it
  // allows to pre-filter the images, and makes it possible to deal with
  // multiple level of details (ie multi-resolution pyramid)

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);

  cv::Mat image = cv::imread("../resources/color.png");
  if (image.empty())
  {
    std::cout << "image empty" << std::endl;
  }
  else
  {
    glTexImage2D(
        GL_TEXTURE_2D,  // Type of texture
        0,  // Pyramid level (for mip-mapping) - 0 is the top level (highest
            // resolution)
        GL_RGB,      // Internal colour format to convert to
        image.cols,  // Image width  i.e. 640 for Kinect in standard mode
        image.rows,  // Image height i.e. 480 for Kinect in standard mode
        0,           // Border width in pixels (can either be 1 or 0)
        GL_BGR,      // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
        GL_UNSIGNED_BYTE,  // Image data type
        image.ptr());      // The actual image data itself

    glGenerateMipmap(GL_TEXTURE_2D);
  }

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, tex[1]);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);

  cv::Mat image2 = cv::imread("../resources/color1.png");
  if (image2.empty())
  {
    std::cout << "image empty" << std::endl;
  }
  else
  {
    glTexImage2D(
        GL_TEXTURE_2D,  // Type of texture
        0,  // Pyramid level (for mip-mapping) - 0 is the top level (highest
            // resolution)
        GL_RGB,       // Internal colour format to convert to
        image2.cols,  // Image width  i.e. 640 for Kinect in standard mode
        image2.rows,  // Image height i.e. 480 for Kinect in standard mode
        0,            // Border width in pixels (can either be 1 or 0)
        GL_BGR,       // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
        GL_UNSIGNED_BYTE,  // Image data type
        image2.ptr());     // The actual image data itself

    glGenerateMipmap(GL_TEXTURE_2D);
  }

  const GLint uniIntensity = 0;
  glUniform1f(uniIntensity, 1.f);

  /**
   * Event loop for SFML window
   **/
  auto t_start = std::chrono::high_resolution_clock::now();
  bool running = true;
  while (running)
  {
    sf::Event windowEvent;
    while (window.pollEvent(windowEvent))
    {
      switch (windowEvent.type)
      {
        case sf::Event::Closed:
          running = false;
          break;
        case sf::Event::KeyPressed:
          if (windowEvent.key.code == sf::Keyboard::Escape) running = false;
          break;
        default:
          break;
      }
    }

    // Clear the screen to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    auto t_now = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration_cast<std::chrono::duration<float>>(
                     t_now - t_start).count();
    // Modify the fragement shader uniform for intensity
    // This will cause the triangle to gradually appear and disappear over time
    glUniform1f(uniIntensity, (sin(time / 2.f) + 1.f) / 2.f);
    // Draws the currently bound VAO using the currently bound shader program.
    // This time we use 6 indices of the element array for the drawing
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    window.display();
  }

  glDeleteTextures(2, tex);

  // Clean up
  glDeleteProgram(shaderProgram);
  glDeleteShader(fragmentShader);
  glDeleteShader(vertexShader);
  glDeleteBuffers(1, &vbo);
  glDeleteVertexArrays(1, &vao);

  return 0;
}
