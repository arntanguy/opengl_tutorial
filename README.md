# Modern OpenGL Tutorial

This is an example-driven tutorial for OpenGL, with a strong focus towards implementing
real-time vision algorithms. This tutorial deals with code only, but extensive comments will
be present, along with references to the ressources required to understand the concepts 
introduced along the tutorials.

The code will be devoid of deprecated features, only modern OpenGL 4.3+ features will be used.

## References

This tutorial is strongly inspired by the following (recommended) ressources

- https://open.gl
- http://ogldev.atspace.co.uk/index.html
- http://www.learnopengl.com/


## Documentation

You will find documentation for all the introduced OpenGL features on 
- http://docs.gl/ : improved version of the official documentation
- https://www.opengl.org/sdk/docs/man/ : official opengl documentation


## Dependencies

- CMake (>=3.1)
- SFML 2.5
- Glew
- OpenGL >4.5

**Notes on SFML:**

By default on Ubuntu 18.04, only SFML 2.4 is installed. While this package could work with it, modern cmake targets for SFML were only introduced in SFML 2.5. To install it, you'll need to compile SFML, fortunately, that's easy:

```
git clone https://github.com/SFML/SFML.git
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j8
sudo make install
```

You will need some dependencies all installable from ubuntu packages.



## Running the samples

```
mkdir build
cd build
cmake ..
make
./001_context_creation/src/context_creation
...
```
