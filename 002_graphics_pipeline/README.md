= Basic use of the rendering pipeline

This tutorial is entirely based, with minor improvements on https://open.gl/drawing 
Please refer to it for detailled explanations.

This example contains 3 related programs:

- render_triangle.cpp deals with creating VBOs and VAOs, and rendering them using shaders.
- render_triangle_uniforms.cpp improves on the previous one by introducing uniform variable to control the shader
behavior
- render_element_array.cpp shows an alternative way of rendering using Element Array Buffers, which are more
efficient when a lot of vertices are shared between triangles.
