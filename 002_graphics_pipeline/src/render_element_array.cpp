#include <GL/glew.h>
#include <SFML/Window.hpp>

#include <iostream>
#include <chrono>
#include <cmath>

int main(int argc, char *argv[])
{
  // Settings for OpenGL context
  sf::ContextSettings settings;
  settings.depthBits = 24;
  settings.stencilBits = 8;
  settings.antialiasingLevel = 2;  // Optional
  settings.majorVersion = 4;
  settings.minorVersion = 5;

  // Create Window and context
  sf::Window window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Close,
                    settings);

  // Initialize OpenGL functionalities
  glewExperimental = GL_TRUE;
  glewInit();

  /**
   * Declare the data to be drawn on the CPU
   * Here, we declare a triangle defined by 3 2D vertices.
   **/
  std::array<float, 4 * 5> vertices{{
      -0.5f, 0.5f, 1.0f, 0.0f, 0.0f,  // Top-left
      0.5f, 0.5f, 0.0f, 1.0f, 0.0f,   // Top-right
      0.5f, -0.5f, 0.0f, 0.0f, 1.0f,  // Bottom-right
      -0.5f, -0.5f, 1.0f, 1.0f, 1.0f  // Bottom-left
  }};

  // Element array references the vertices by index
  // 0 is the top-left vertex, 1 the top-right...
  std::array<int, 2 * 3> elements{{0, 1, 2, 2, 3, 0}};

  // Create VAO to store how the data is laid out
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Create EBO (element array buffer)
  GLuint ebo;
  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
    sizeof(elements), elements.data(), GL_STATIC_DRAW);

  // Create VBO
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(),
               GL_STATIC_DRAW);

  /**
   * DEFINING SHADERS - Rendering pipeline
   **/

  // 1 - VERTEX SHADER

  std::string vertexSource =
      "#version 440\n"
      "layout(location=0) in vec2 position;"
      "layout(location=1) in vec3 color;"
      "out vec3 Color;"
      "void main()"
      "{"
      "    Color = color;"
      "    gl_Position = vec4(position, 0.0, 1.0);"
      "}";
  const char *vertexSource_str = vertexSource.c_str();

  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexSource_str, NULL);
  glCompileShader(vertexShader);

  {  // debug shader compilation
    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE)
    {
      char buffer[512];
      glGetShaderInfoLog(vertexShader, 512, NULL, buffer);
      std::cerr << "Error compiling vertex shader: " << buffer << std::endl;
    }
  }

  // 2 - FRAGMENT SHADER
  std::string fragmentSource =
      "#version 440\n"
      "in vec3 Color;"
      "out vec4 outColor;"
      "uniform float intensity;"
      "void main()"
      "{"
      "		outColor = intensity*vec4(Color, 1.0);"
      "}";
  const char *fragmentSource_str = fragmentSource.c_str();

  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentSource_str, NULL);
  glCompileShader(fragmentShader);

  {  // debug shader compilation
    GLint status;
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE)
    {
      char buffer[512];
      glGetShaderInfoLog(fragmentShader, 512, NULL, buffer);
      std::cerr << "Error compiling fragment shader: " << buffer << std::endl;
    }
  }

  // 3 - PROGRAM
  GLuint shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glBindFragDataLocation(shaderProgram, 0, "outColor");
  glLinkProgram(shaderProgram);
  glUseProgram(shaderProgram);

  // layout id of the position attribute in the vertex shader.
  const GLint posAttrib = 0;
  std::cout << "Pose attrib: " << glGetAttribLocation(shaderProgram, "position")
            << std::endl;
  ;
  const GLint colorAttrib = 1;
  std::cout << "Color attrib: " << glGetAttribLocation(shaderProgram, "color")
            << std::endl;
  ;

  /**
   * VERTEX ATTRIBUTE POINTERS
   * Vertex Attribute pointers are at first sight rather confusing.
   * Their role is to specify how the input data is laid out.
   * They specify how to use data in the currently bound VBO
   * (here "vbo" containing our triangle vertices).
   * It takes as parameters:
   *  - The reference to the input attribute
   *  - The number of values for that input, which is the same as the number of
   *    compoments of the vec
   *  - The type of each component (GL_FLOAT)
   *  - Whether each component should be normalized between -1 and 1
   *  - The stride: how many bytes are between each position attribute
   *    in the array
   *  - The offset: how many bytes from the start of the array
   *    the attribute occurs
   *
   * IMPORTANT NOTE: this function will not only store the stride and offset,
   * but also the VBO currently bound to GL_ARRAY_BUFFER.
   * This means the the VBO doesn't have to be explicitely bound when the actual
   * drawing functions are called.
   * This also means that you can have a different VBO for each attribute.
   **/

  // We just need to alter the attribute pointer code a bit to accommodate for
  // the new
  // X, Y, R, G, B attribute order.
  // The fifth parameter is set to 5*sizeof(float) now,
  // because each vertex consists of 5 floating point attribute values.
  // The offset of 2*sizeof(float) for the color attribute is there because each
  // vertex starts
  // with 2 floating point values for the position that it has to skip over.

  // Enable the vertex attribute
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
  // Enable the color attribute
  glEnableVertexAttribArray(colorAttrib);
  glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                        (void *)(2 * sizeof(float)));

  /**
   * Uniforms are essentially global variable that have the same
   * values for all vertices and/or fragments.
   * They can be used to influence the behavior of shaders.
   *
   * To demonstrate this, we added an "intensity" uniform that alters
   * the color intensity over time
   */
  // Get reference to the uniform. It's value will be modified in the rendering
  // loop
  // below
  GLint uniIntensity = glGetUniformLocation(shaderProgram, "intensity");

  /**
   * Event loop for SFML window
   **/
  auto t_start = std::chrono::high_resolution_clock::now();
  bool running = true;
  while (running)
  {
    sf::Event windowEvent;
    while (window.pollEvent(windowEvent))
    {
      switch (windowEvent.type)
      {
        case sf::Event::Closed:
          running = false;
          break;
        case sf::Event::KeyPressed:
          if (windowEvent.key.code == sf::Keyboard::Escape) running = false;
          break;
        default:
          break;
      }
    }

    // Clear the screen to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    auto t_now = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration_cast<std::chrono::duration<float>>(
                     t_now - t_start).count();
    // Modify the fragement shader uniform for intensity
    // This will cause the triangle to gradually appear and disappear over time
    glUniform1f(uniIntensity, (std::sin(time * 4.f) + 1.f) / 2.f);

    // Draws the currently bound VAO using the currently bound shader program.
    // This time we use 6 indices of the element array for the drawing
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    window.display();
  }

  // Clean up
  glDeleteProgram(shaderProgram);
  glDeleteShader(fragmentShader);
  glDeleteShader(vertexShader);
  glDeleteBuffers(1, &vbo);
  glDeleteVertexArrays(1, &vao);

  return 0;
}
