#include <GL/glew.h>
#include <SFML/Window.hpp>

#include <iostream>

int main(int argc, char *argv[])
{
  // Settings for OpenGL context
  sf::ContextSettings settings;
  settings.depthBits = 24;
  settings.stencilBits = 8;
  settings.antialiasingLevel = 2;  // Optional
  settings.majorVersion = 4;
  settings.minorVersion = 5;

  // Create Window and context
  sf::Window window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Close,
                    settings);

  /**
   * The graphics card vendor is responsible to implement OpenGL
   * functionnalities in their drivers based on what the graphics card supports.
   * GLEW takes care of initializing all the required function pointers to
   * the actual driver implementation. This is required to use any
   * modern OpenGL features
   */
  glewExperimental = GL_TRUE;
  glewInit();

  /**
   * Declare the data to be drawn on the CPU
   * Here, we declare a triangle defined by 3 2D vertices.
   **/
  std::array<float, 6> vertices = {
      0.0f, 0.5f,   // Vertex 1 (X, Y)
      0.5f, -0.5f,  // Vertex 2 (X, Y)
      -0.5f, -0.5f  // Vertex 3 (X, Y)
  };

  /**
	 * UPLOADING DATA to a VBO on the GPU
   * The data declared on the CPU now needs to be passed on to the GPU.
   * One important thing to note, is that transfering data from the CPU to the
   * GPU is in general costly. However, the data bus is designed to handle well
   * streaming data (ie sending continuous large chunks of memory).
   *
   * Thus, you want to limit data transfer from CPU to GPU to an absolute
   * minimum. For that purpose, Vertex Buffer Objects (VBO) may be used. A VBO
   * is a chunk of memory on the graphics card mananged by the OpenGL driver.
   *
   * Contrary to what the name seems to imply, VBOs are not only useful for
   * storing vertices, but any kind of continuous data. Think of it as a pointer
   * to a chunk of memory on the GPU.
   **/

  GLuint vbo;
  // Generate 1 vertex buffer object
  // As the memory is mananged by opengl,instead of a pointer, the API returns a
  // positive number (vbo) as a reference to it. This number will be used by
  // opengl API calls to refer to the VBO's memory
  glGenBuffers(1, &vbo);

  // OpenGL is a state machine.
  // In order to manipulate the newly created buffer, you need to make it
  // active, so that all following actions will be applied to that buffer.
  // GL_ARRAY_BUFFER is but one of many buffer types. This has to do with
  // the way the memory is laid out on the GPU, allowing for driver optimization
  // depending on the role of your data. Ignore this for now.
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  // Upload our vertex array to the active ARRAY_BUFFER. Notice that
  // the VBO id isn't used here. It is unecessary, as the buffer is currently
  // bound, so any action performed in that state affects the VBO.
  //
  // GL_STATIC_DRAW is once again a flag giving hints to the driver as to what
  // we intend to do with the data. GL_STATIC_DRAW is used to indicate that the
  // data will be uploaded once and drawn many times.
  // Other flags include GL_DYNAMIC_DRAW (data changed from time to time, drawn
  // many times more), GL_STREAM_DRAW (data changes almost every time it's
  // drawn, ie video, user interface...).
  // Note that this flag's value does not in any way prevent us from modifying
  // the data, it's only there for driver optimization purposes.
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(),
               GL_STATIC_DRAW);

  /**
   * DEFINING SHADERS - Rendering pipeline
   * Our triangle vertices have now been uploaded to a VBO on the GPU.
   * It is now time to set up a rendering pipeline to display them.
   * To do so, we will use the vertex and fragment shaders part of the rendering
   * pipeline.
   *
   **/

	// 1 - VERTEX SHADER

  std::string vertexSource =
      "#version 440\n"
      "layout(location=0) in vec2 position;\n"
      "void main()"
      "{"
      "    gl_Position = vec4(position, 0.0, 1.0);"
      "}";
  const char *vertexSource_str = vertexSource.c_str();

  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  // The last parameter is the length of the strings. If NULL,
  // the strings are assumed to be null terminated.
  glShaderSource(vertexShader, 1, &vertexSource_str, NULL);
  // Compiles the shader source as an executable compatible with the current
  // GPU architecture
  glCompileShader(vertexShader);

  // WARNING: if the shader fails to compile, glGetError will not report an
  // error
  // The code block below deals with checking for compilation errors
  {// debug shader compilation
    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE)
    {
      char buffer[512];
      glGetShaderInfoLog(vertexShader, 512, NULL, buffer);
			std::cerr << "Error compiling vertex shader: " << buffer << std::endl;
    }
  }

  // 2 - FRAGMENT SHADER
  std::string fragmentSource =
      "#version 440\n"
      "out vec4 outColor;\n"
      "void main()\n"
      "{\n"
      "		outColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
      "}";
  const char *fragmentSource_str = fragmentSource.c_str();

  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentSource_str, NULL);
  glCompileShader(fragmentShader);

  {// debug shader compilation
    GLint status;
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE)
    {
      char buffer[512];
      glGetShaderInfoLog(fragmentShader, 512, NULL, buffer);
			std::cerr << "Error compiling fragment shader: " << buffer << std::endl;
    }
  }

	// 3 - PROGRAM
	// So far, We defined a vertex, and fragment shader independently.
	// However, they are meant to be connected, to do so, we need to create a shader program,
	// to which the vertex and fragment shader are attached.
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	// Connect the shaders of the program together
	// It is allowed to make change to the shaders after they've been added to a program,
	// but the result will not be effective until they are linked again
	glLinkProgram(shaderProgram);


	/**
	 * Using the shader program
	 */

	// 1 - Activate the program
	// similar to glBindBuffer, but for a program
	glUseProgram(shaderProgram);

	// 2 - Making the link between vertex data and attributes

	// layout id of the position attribute in the vertex shader.
	const GLint posAttrib = 0;
	// Note: the attribute ID can also be obtained using the following.
	// However, this is slower and less convenient than defining layout attributes in the shader itself
 	//GLint posAttrib = glGetAttribLocation(shaderProgram, "position");

	/**
	 * OPTIONAL: vertex array objects
	 *  VAOs store all of the links between your attributes and the raw vertex data
	 *  If that step is ommited, the default VAO will be used, but you will have to specify again
	 *  all of the rules for laying out the data again everytime you want to use your shader.
   */
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);


	/**
	 * VERTEX ATTRIBUTE POINTERS
	 * Vertex Attribute pointers are at first sight rather confusing.
	 * Their role is to specify how the input data is laid out.
   * They specify how to use data in the currently bound VBO (here "vbo" containing our
	 * triangle vertices).
	 * It takes as parameters:
	 *  - The reference to the input attribute
	 *  - The number of values for that input, which is the same as the number of compoments of the vec
   *  - The type of each component (GL_FLOAT)
	 *  - Whether each component should be normalized between -1 and 1
	 *  - The stride: how many bytes are between each position attribute in the array
   *  - The offset: how many bytes from the start of the array the attribute occurs
	 *
	 * IMPORTANT NOTE: this function will not only store the stride and offset,
	 * but also the VBO currently bound to GL_ARRAY_BUFFER. This means the the VBO doesn't have
	 * to be explicitely bound when the actual drawing functions are called.
	 * This also means that you can have a different VBO for each attribute.
   **/
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
	// Enable the vertex attribute
	glEnableVertexAttribArray(posAttrib);



  /**
   * Event loop for SFML window
   **/
  bool running = true;
  while (running)
  {

    sf::Event windowEvent;
    while (window.pollEvent(windowEvent))
    {
      switch (windowEvent.type)
      {
        case sf::Event::Closed:
          running = false;
          break;
        case sf::Event::KeyPressed:
          if (windowEvent.key.code == sf::Keyboard::Escape) running = false;
          break;
        default:
          break;
      }
    }

    // Clear the screen to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

		// Draws the currently bound VAO using the currently bound shader program.
		glDrawArrays(GL_TRIANGLES, 0, 3);

    window.display();
  }

  // Clean up
  glDeleteProgram(shaderProgram);
  glDeleteShader(fragmentShader);
  glDeleteShader(vertexShader);
  glDeleteBuffers(1, &vbo);
  glDeleteVertexArrays(1, &vao);

  return 0;
}
